import glob
from datetime import datetime
import os
from api import get_url, get_graph_string

SENTENCES = 'sentences'


class Runner(object):
    def __init__(self, data_path):
        self.data_path = data_path
        self.input_files = glob.glob(f"{self.data_path}/*.txt")
        self.input_files_names = [
            x.replace(self.data_path, '').replace('\\', '').replace('.txt', '')
            for x in self.input_files]
        self.runner_birth_time = datetime.now().strftime('%H-%M-%S')
        self.output_dir = f"{self.data_path}/outputs_runner_{self.runner_birth_time}"
        self.sub_dirs = [SENTENCES]

    def initialize(self):
        for sub_dir in self.sub_dirs:
            if not os.path.exists(f"{self.output_dir}/{sub_dir}"):
                os.makedirs(f"{self.output_dir}/{sub_dir}")

    def get_sentences(self):
        directory = f'{self.output_dir}/{SENTENCES}'
        for file in self.input_files:
            file_id = [fid for fid in self.input_files_names if fid in file][0]
            print(file_id)
            with open(file, 'r') as f:
                with open(f'{directory}/{file_id}', 'w') as inp:
                    for line in f.readlines():
                        inp.write(','.join(line.split(',')[1:])[1:])

    def create_submit(self):
        for file in self.input_files:
            processed = []
            with open(file, 'r') as input_txt:
                for line in input_txt.readlines():
                    id = line.split(',')[0]
                    sent = ','.join(line.split(',')[1:])
                    url = get_url(sent)
                    graph = get_graph_string(url)
                    processed.append(f"{id}, {graph}")
            file_id = file[58:-4]
            with open(f"{self.output_dir}\{file_id}.txt", "w") as f:
                for o_line in processed:
                    f.write(o_line)
                    f.write('\n')
            print(f"Finished : {file_id}")

