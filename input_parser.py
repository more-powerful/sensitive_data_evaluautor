import glob


def get_input_files():
    mylist = [f for f in glob.glob("./data/sh1-1/training/*.txt")]

    sentences = []
    reading_graph = False
    graph = ""
    sentence = ""

    for file_path in mylist:

        with open(file_path, 'r', encoding="utf-8") as file:
            lines = file.readlines()

        for no, line in enumerate(lines):
            if "id" in line[4:6]:
                sentences.append((sentence, graph))
                reading_graph = False
                sentence = ""
                graph = ""


            elif "snt" in line[4:7]:
                sentence += line[8:-1]

            elif "save" in line[4:8]:
                reading_graph = True

            elif reading_graph:
                graph += line

            elif line == "\n":
                continue

    return sentences

