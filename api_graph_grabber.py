#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests

from bs4 import BeautifulSoup
from numpy import unicode


def get_url(sent):
    sent = sent.split(' ')
    sent = '+'.join(sent)
    api_call = "http://bollin.inf.ed.ac.uk:9010/amreager?callback=jQuery111009662606117659124_1542455433003&lang=en&sent={}&_=1542455433005".format(
        sent)
    return api_call


def get_sent(elem):
    if isinstance(elem, str) and elem.startswith("jQuery"):
        elem = elem.split('"graph"')[1][11:]
        return elem


def only_graph(elem):
    if not isinstance(elem, str):
        return None
    if isinstance(elem, str) and (elem.startswith("jQuery") or elem.startswith(
            "# ::alignments")):
        return None
    else:
        return elem


def get_graph_string(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    cont = soup.contents
    parsed = []
    for elem in cont:
        if '"link"' in elem or '"png"' in elem:
            continue
        elem = only_graph(elem)

        if elem:
            elem = elem.replace(u'\xa0', u' ').replace(u'\\"', u'"')
            parsed.append(elem)
    return ''.join([e for e in parsed])


def get_graph(url, file_name):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    cont = soup.contents
    parsed = []
    for elem in cont:
        if '"link"' in elem or '"png"' in elem:
            continue
        elem = only_graph(elem)

        if elem:
            elem = elem.replace(u'\xa0', u' ').replace(u'\\"', u'"')
            # print(elem)
            parsed.append(elem)
            # print(parsed)
    with open(f"{file_name}.txt", "a") as f:
        for e in parsed:
            f.write(e)
            f.write('\n')
        f.write('\n')