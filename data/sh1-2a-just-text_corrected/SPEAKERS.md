sbc0001 	neighborhood:  	outskirts of small town
sbc0001 	room:          	living room of house trailer; hot afternoon
sbc0001 	event_type:    	face to face conversation
sbc0001 	event:         	mother and daughter catching up after daughter's absence
sbc0001 	prior_events:  	daughter returned that day from a months vacation in Minnesota
sbc0001 	num_spkrs:     	3
sbc0001 	speaker:      	0001
sbc0001 	speaker:      	0002
sbc0001 	speaker:      	0003
sbc0002 	neighborhood:  	-
sbc0002 	room:          	private home
sbc0002 	event_type:    	face to face conversation
sbc0002 	event:         	-
sbc0002 	prior_events:  	-
sbc0002 	num_spkrs:     	4
sbc0002 	speaker:      	0004
sbc0002 	speaker:      	0005
sbc0002 	speaker:      	0006
sbc0002 	speaker:      	0007
sbc0003 	neighborhood:  	-
sbc0003 	room:          	private home, kitchen
sbc0003 	event_type:    	face to face conversation
sbc0003 	event:         	-
sbc0003 	prior_events:  	-
sbc0003 	num_spkrs:     	3
sbc0003 	speaker:      	0007
sbc0003 	speaker:      	0008
sbc0003 	speaker:      	0009
sbc0004 	neighborhood:  	downtown
sbc0004 	room:          	private home, kitchen
sbc0004 	event_type:    	face to face conversation
sbc0004 	event:         	drinking coffee
sbc0004 	prior_events:  	got up
sbc0004 	num_spkrs:     	6
sbc0004 	speaker:      	0010
sbc0004 	speaker:      	0011
sbc0004 	speaker:      	0012
sbc0004 	speaker:      	0013
sbc0004 	speaker:      	0014
sbc0004 	speaker:      	0015
sbc0005 	neighborhood:  	private home in Santa Barbara
sbc0005 	room:          	private home, bedroom
sbc0005 	event_type:    	face to face conversation
sbc0005 	event:         	chatting in bed
sbc0005 	prior_events:  	-
sbc0005 	num_spkrs:     	2
sbc0005 	speaker:      	0016
sbc0005 	speaker:      	0017
sbc0006 	neighborhood:  	Los Angeles
sbc0006 	room:          	living room
sbc0006 	event_type:    	face to face conversation
sbc0006 	event:         	2 participants hadn't seen each other for several weeks
sbc0006 	prior_events:  	-
sbc0006 	num_spkrs:     	2
sbc0006 	speaker:      	0001
sbc0006 	speaker:      	0018
sbc0007 	neighborhood:  	rural housing project - Indian reservation
sbc0007 	room:          	living room
sbc0007 	event_type:    	face to face conversation
sbc0007 	event:         	sisters talking about problems, a friend who is dying in the hospital, kid
sbc0007 	prior_events:  	-
sbc0007 	num_spkrs:     	2
sbc0007 	speaker:      	0019
sbc0007 	speaker:      	0020
sbc0008 	neighborhood:  	downtown
sbc0008 	room:          	attorney's office
sbc0008 	event_type:    	interview/task-related talk
sbc0008 	event:         	trial preparation
sbc0008 	prior_events:  	-
sbc0008 	num_spkrs:     	4
sbc0008 	speaker:      	0021
sbc0008 	speaker:      	0022
sbc0008 	speaker:      	0023
sbc0008 	speaker:      	0024
sbc0009 	neighborhood:  	-
sbc0009 	room:          	family room
sbc0009 	event_type:    	task related talk/face to face conversation
sbc0009 	event:         	girl helps boyfriend with math problems
sbc0009 	prior_events:  	-
sbc0009 	num_spkrs:     	2
sbc0009 	speaker:      	0025
sbc0009 	speaker:      	0026
sbc0010 	neighborhood:  	downtown
sbc0010 	room:          	office
sbc0010 	event_type:    	face to face conversation
sbc0010 	event:         	talking about political problems of an arts organization
sbc0010 	prior_events:  	nasty letter to editor, morning at an arts organization festival
sbc0010 	num_spkrs:     	3
sbc0010 	speaker:       	0011
sbc0010 	speaker:      	0027
sbc0010 	speaker:      	0028
sbc0011 	neighborhood:  	-
sbc0011 	room:          	around kitchen table
sbc0011 	event_type:    	face to face conversation
sbc0011 	event:         	face to face conversation
sbc0011 	prior_events:  	-
sbc0011 	num_spkrs:     	3
sbc0011 	speaker:      	0029
sbc0011 	speaker:      	0030
sbc0011 	speaker:      	0031
sbc0012 	neighborhood:  	-
sbc0012 	event_type:    	teaching/discussion
sbc0012 	event:         	teacher discussing history of Chicano Studies to students
sbc0012 	prior_events:  	-
sbc0012 	num_spkrs:     	9
sbc0012 	speaker:      	0032
sbc0012 	speaker:      	0033
sbc0012 	speaker:      	0034
sbc0012 	speaker:      	0035
sbc0012 	speaker:      	0036
sbc0012 	speaker:      	0037
sbc0012 	speaker:      	0038
sbc0012 	speaker:      	0039
sbc0012 	speaker:      	0040
sbc0013 	neighborhood:  	Ft. Wayne, IN home
sbc0013 	room:          	in the dining room in a home
sbc0013 	event_type:    	face to face conversation
sbc0013 	event:         	dinner party for Kendra's birthday, dinner, opening of gifts, cake & singing
sbc0013 	prior_events:  	preparation for dinner, visiting with friends
sbc0013 	num_spkrs:     	5
sbc0013 	speaker:      	0042
sbc0013 	speaker:      	0043
sbc0013 	speaker:      	0044
sbc0013 	speaker:      	0045
sbc0013 	speaker:      	0046
sbc0014 	neighborhood:  	rural downtown neighborhood, middle-class
sbc0014 	room:          	conference room
sbc0014 	event_type:    	task-related talk
sbc0014 	event:         	loan officer and 2 board members are having a loan meeting
sbc0014 	prior_events:  	-
sbc0014 	num_spkrs:     	5
sbc0014 	speaker:      	0047
sbc0014 	speaker:      	0048
sbc0014 	speaker:      	0049
sbc0014 	speaker:      	0050
sbc0014 	speaker:      	0051

------------------------


0001,LENORE,f,30
0002,DORIS,f,50
0003,LYNNE,f,19
0004,HAROLD,m,38
0005,JAMIE,f,30
0006,MILES,m,38
0007,PETE,m,36
0008,ROY,m,34
0009,MARILYN,f,33
0010,CAROLYN,f,19
0011,KATHY,f,31
0012,SHARON,f,24
0013,SHANE,m,23
0014,PAM,f,43
0015,WARREN,m,34
0016,DARRYL,m,33
0017,PAMELA,f,38
0018,ALINA,f,34
0019,ALICE,f,28
0020,MARY,f,27
0021,RICKIE,f,28
0022,JUNE,f,21
0023,REBECCA,f,31
0024,ARNOLD,m,28
0025,KATHY,f,17
0026,NATHAN,m,19
0027,BRAD,m,45
0028,PHIL,m,30
0029,DORIS,f,83
0030,ANGELA,f,90
0031,SAM,f,72
0032,BEV,f,20
0033,MONTOYO,m,51
0034,MARIA,f,26
0035,GILBERT,m,22
0036,CAROLYN,f,18
0037,LAURA,f,23
0038,FRANK,m,24
0039,RAMON,m,19
0040,RUBEN,m,27
0042,KENDRA,f,25
0043,KEN,m,51
0044,MARCI,f,50
0045,WENDY,f,26
0046,KEVIN,m,26
0047,JIM,m,41
0048,FRED,m,47
0049,JOE,m,45
0050,KURT,m,70
0051,VIVIAN,f,55
