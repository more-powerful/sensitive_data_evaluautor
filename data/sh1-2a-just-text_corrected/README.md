This Dataset contain 10 train speech, 2 dev and 2 test files of Spoken American English.

Each speech file is accompanied by a transcript in which phrases are time
stamped with respect to the audio recording.  Personal names, place names,
phone numbers, etc, in the transcripts have been altered to preserve the
anonymity of the speakers and their acquaintances and the audio files have been
filtered to make these portions of the recordings unrecognizable.  Pitch
information is still recoverable from these filtered portions of the
recordings, but the amplitude levels in these regions have been reduced
relative to the original signal.  A separate filter list file (*.flt)
associated with each transcript/waveform file pair is provided to list the
beginning and ending times of the filtered regions.

The filtering was done using a digital FIR low-pass filter, with the
cut-off frequency set at 400 Hz.  The effect of the filter was gradually
faded in and out at the beginning and end of the regions over a 1,000 sample
region, roughly 45 milliseconds, to avoid abrupt transitions in the
resulting waveform.

In the case of a phone number, which was not adequately disguised by the
filter, the signal was set to zero, except for the 45 millisecond boundary
regions which fade into and out of zero.

Each volume contains this README.doc, a file called table.doc which lists the
contents of all three volumes, a "speech" directory containing the waveform,
transcript, and filter list files, and a "doc" directory. Please note that the
table (*.tbl), filter (*.flt), transcript (*.trn), and document (*.doc) files
are suitable for text readers on Unix computers and by word processing software
on MacIntosh (e.g. Word) and PC-Compatible (e.g. WordPad) computers.

For the latest information on this corpus, please refer to the UCSB and
Linguistic Data Consortium (LDC) web sites devoted to it:

	http://humanitas.ucsb.edu/depts/linguistics/research/csae/

	http://www.ldc.upenn.edu/Publications/SBC/

These sites may also contain software or revised versions of data which may
be downloaded.

LICENCE: For the #1Skyhacks Hackathon / competition usage only.