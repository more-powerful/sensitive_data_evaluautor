# Hackathon Skyhacks #1
######  Gliwice, 16-18 listopada 2018

### Analiza dzwięku oraz tekstu

W naszym zadaniu zajęliśmy się tworzeniem grafów AMR ([Abstract Meaning Representation](https://amr.isi.edu/index.html)). W tym celu wykorzystaliśmy model C-AMR ([A transition-based AMR Parser](https://github.com/c-amr/camr)) wyuczony na 2 różnych datasetach: LDC2014T12 oraz SemEval2016 oraz model [AMREager](http://cohort.inf.ed.ac.uk/amreager.html). W projekcie wykorzystaliśmy skrypty Basha, Pythona 2.7, Pythona 3.6.
Po porównaniu wyników oraz zwracanych formatów plików ostatecznie zdecydowaliśm się na model AMREager, których cechował się drugim wynikiem skuteczności w metryce Swatch (odrobinę gorszy od C-AMR SemEval 2016), lecz nie posiadał błędów przetwarzania tekstów na grafych (różna liczba grafów w stosunku do liczby zdań wejściowych).

##### Autorzy (More Powerful):
Łukasz Bombała
Mateusz Szczepański
Patryk Wielopolski
Kamil Zawistowski

