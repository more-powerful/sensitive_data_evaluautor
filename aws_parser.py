import csv
import json
import time

import speech_recognition as sr
from googleapiclient.errors import HttpError

# track_13_schedule = []
# with open('./data/times13.csv', 'r') as schedule13:
#     reader = csv.reader(schedule13, delimiter=',')
#     for row in reader:
#         track_13_schedule.append((row[3], row[2]))
#
# track_14_schedule = []
# with open('./data/times14.csv', 'r') as schedule14:
#     reader = csv.reader(schedule14, delimiter=',')
#     for row in reader:
#         track_14_schedule.append((row[3], row[1], row[2]))
#
#
# with open('skyhacks-3193a4cde164.json') as json_data:
#     credentials_json = json.load(json_data)
#     credentials_json = json.dumps(credentials_json)
#
#
# last_duration = 0
# AUDIOFILE = sr.AudioFile('D:\Projekty\SpeakerRecognition\data\sh1-2a-test\speech-test\sbc0013.wav')
# with AUDIOFILE as source:
#     with open('mycsvfile.txt', 'w') as f:
#         # w = csv.writer(f, delimiter=',')
#         for row in track_14_schedule:
#
#             end_time = float(row[2])
#             start_time = float(row[1])
#             speaker = row[0]
#             print(start_time, end_time, end_time-start_time)
#             r = sr.Recognizer()
#             audio = r.record(source, duration=end_time, offset=start_time)
#
#             try:
#                 result = r.recognize_google_cloud(audio, credentials_json)
#             except sr.RequestError:
#                 print("KURKA EXCEPT")
#                 result =''
#             # print(result)
#             last_duration = end_time
#             f.write(f'{speaker},{result}')
#
#
# print("KONIEC")
# k = 1
#
#
#

with open('./data/aws/14.json', "r") as file:
    current_json = file.read()
    current_json = json.loads(current_json)

    speakers_division = current_json['results']['speaker_labels']['segments']
    words = current_json['results']['items']

sentences = []
sentence = ''
current_speaker = ''

k = 0
for speaker in speakers_division:
    k += 1

    if current_speaker != speaker['speaker_label']:
        current_speaker = speaker['speaker_label']
        sentences.append({current_speaker: sentence})
        sentence = ''

    for word in words:
        content = word['alternatives'][0]['content']
        if len(content) == 1:
            if not (65 < ord(content) < 90) or not (97 < ord(content) < 122):
                continue

        if word['start_time'] >= speaker['start_time'] and word['end_time'] <= speaker['end_time']:
            sentence += content.lower() + ' '
            # del words[num]

    with open('14.txt', 'w') as f:

        for row in sentences:
            for line in row.values():
                f.write(line + '\n')

