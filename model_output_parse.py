from datetime import datetime
import glob


class Parser(object):
    def __init__(self, model_output_path, input_text_path):
        self.data_path = model_output_path
        self.data_path_test = input_text_path
        self.input_files = glob.glob(f"{self.data_path}/*.parsed")
        self.input_files_names = [
            x.replace(self.data_path, '').replace('\\', '').replace('.txt', '')
            for x in self.input_files]

        self.input_test_files = glob.glob(f"{self.data_path_test}/*.txt")
        self.input_test_files_names = [
            x.replace(self.data_path_test, '').replace('\\', '').replace('.txt',
                                                                         '')
            for x in self.input_test_files]
        self.parser_birth_time = datetime.now().strftime('%H-%M-%S')
        self.output_dir = f"{self.data_path}/outputs_parser_{self.parser_birth_time}"

    @staticmethod
    def extract_graphs(output):
        break_ids = [i for i, x in enumerate(output) if x.startswith("#")]
        pairs = []
        all = []
        graphs = []
        for id in break_ids:
            if len(pairs) <= 1:
                pairs.append(id)
            else:
                all.append(pairs)
                pairs = []
                pairs.append(id)
        for i, pair in enumerate(all):
            id = pair[1]
            rows = [output[id] for id in range(id + 1, all[
                i + 1 if i + 1 < len(all) else len(all) - 1][0] - 1)]
            graphs.append(''.join(rows).replace('\t', ' ').replace('\n', ' '))
        return graphs

    @staticmethod
    def extract_id(input):
        lines = []
        for line in input:
            id = line.split(',')[0]
            lines.append(id)
        return lines

    def process_csv(self):
        files_model = self.input_files
        bare_files = self.input_test_files_names
        files_test = self.input_test_files
        for bare_file in bare_files:
            print(bare_file)
            if any(bare_file in f for f in files_model) and any(
                            bare_file in f for f in files_test):
                with open(f'{self.output_dir}_{bare_file}.csv', 'w') as otp:
                    print([x for x in files_model if bare_file in x][0])
                    print([x for x in files_test if bare_file in x][0])
                    with open([x for x in files_model if bare_file in x][0],
                              'r') as output:
                        with open([x for x in files_test if bare_file in x][0],
                                  'r') as input:
                            ids = self.extract_id(input.readlines())
                            graphs = self.extract_graphs(output.readlines())
                            print(len(ids), len(graphs))
                            for i, id in enumerate(ids):
                                print(id, graphs[i if i < len(graphs) else len(graphs) - 1])
                                otp.write(
                                    f"{id},{graphs[i if i < len(graphs) else len(graphs) - 1]}")
                                otp.write('\n')

